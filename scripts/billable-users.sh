#!/bin/bash

# Customize these variables with your GitLab URL and your Personal Access Token (admin)
GLHOST="https://gitlab.com"
GLTOKEN="xxxxxxxxxxxxxxxx"

# This is a json containing the whole users list
echo "Retrieving the whole users list"
curl --header "PRIVATE-TOKEN: ${GLTOKEN}" -L -X GET ${GLHOST}/api/v4/users \
	| jq . > result.json

# This is converting the json to a csv, including just few fields
cat result.json | jq -r 'map(.username), map(.email), map(.state), map(.using_license_seat) | @csv' \
    | ruby -rcsv -e 'puts CSV.parse(STDIN).transpose.map &:to_csv' > result.csv
